#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <readline/history.h>
#include <readline/readline.h>


/*
    Welcome to the File Downloader program by Skyler Rader.
    It has basic functionality except that jpgs won't open correctly, and I can't figure out how to fix them.
    It's been a toss up on line 216 whether or not to remove the NULL character, I left it in so that you could see it.
    It makes text files much better since they have NULLs in them sometimes.
    I added a progress bar and an ask to overwrite.
    I realize it doesn't have full functionality, because of the JPGs, but I think it's at least halfway,
    if not more complete. This non-functionality is not due to neglect, I've spent countless hours, and many sleepless night
    s working on it to try to get it full functioning.
    
    Please give this project the amount of points you think it's worth. Thank you.
*/


void menu();
char get_choice();
void download_file(int s);
void list_files(int s);
void close_connection(int s);
int open_connection();
void prog(int count, int max);

int main()
{
    int sockfd = open_connection();
    while(1)
    {
        menu();
        char c = get_choice();
        switch (c)
        {
            case 'l':
            case 'L':
                list_files(sockfd);
                break;
            case 'd':
            case 'D':
                download_file(sockfd);
                break;
            case 'q':
            case 'Q':
                close_connection(sockfd);
                exit(0);
            default:
                printf("Please enter d, l, or q\n");
                break;
        }
    }
}

void menu()
{
    printf("L)ist files\n");
    printf("D)ownload a file\n");
    printf("Q)uit\n");
    printf("\n");
}

char get_choice()
{

    printf("Choice: ");
    char line[20];
    scanf("%s", line );
    
    char first = line[0];
    return first;
}

int open_connection()
{
    struct sockaddr_in sa;
    int sockfd;
    char buf[200];
    // gethostbyname
    struct hostent *he = gethostbyname("runwire.com");
    char *ip = he->h_addr_list[0];
    
    // Fill in socket struct
     
    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);// possibly the port number
    sa.sin_addr = *((struct in_addr *)ip);

    // Create socket
    
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1) {
    fprintf(stderr, "Can't create socket\n");
	exit(3);
    }
    
    // Connect
    int res = connect(sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1) {
        fprintf(stderr, "Can't connect\n");
	exit(2);
    }


    int n = recv(sockfd,buf,sizeof(buf),0);
    if(n < 0)
    {
        printf("ERROR reading");
    }    
    printf("%s", buf);
    
    
    
    // return socket number (file descriptor)
    return sockfd;
}

void list_files(int s)
{

    char buf[1000];
    int n = 0;


    
    // Send LIST command
    sprintf(buf,"LIST\n");
    send(s, buf, strlen(buf), 0);
    
    
    // Receive listing
    memset(buf, '\0', 1000);
    n = recv(s,buf,sizeof(buf),0);
    if(n < 0)
    {
        printf("ERROR reading");
    }
    
    // Display to user
    
    printf("%s", buf);
    printf("\n\n");

}


void download_file(int s)
{
    char name[25];
    char buf[1000];
    int total = 0;
    int n = 0;
    
    int size;
    
    
    // Ask user which file
    printf("What do you desire: ");
    scanf("%s", name);
    
    // Find out how big the file is
    sprintf(buf,"SIZE %s\n", name);
    send(s, buf, strlen(buf), 0);

    
    n = recv(s,buf,sizeof(buf),0);
    if(n < 0)
    {
        printf("ERROR reading");
    }
    

    if(strncmp(buf,"-ERR",3) == 0)
    {
        printf("Only perfect spellers are allowed to download. File not found. Try Again.\n");
        return;
    }
    
    memmove(buf,buf+4,strlen(buf));
    
    
    size = atoi(buf);
    
    if(access( name, F_OK ) != -1 )
    {
        printf("File exists on system. Overwrite? Y/N:");
        char c = get_choice();
        switch (c)
        {
            case 'y':
            case 'Y':
                break;
            case 'n':
            case 'N':
                return;
        }
    }
    
    sprintf(buf,"GET %s\n", name);
    send(s, buf, strlen(buf), 0);
    
    FILE *fp;
    fp = fopen(name, "w+");

    while(total <= size)
    {
            
        memset(buf, '\0', 1000);
        n = recv(s,buf,sizeof(buf),0);
        
        
        if(n < 0)
        {
            printf("ERROR reading");
        }

        if(strncmp(buf,"+OK",3)==0)
        {
            memmove(buf,buf+4,sizeof(buf));
        }
        
        for(int i=0; i<n; i++)
        {
//           if(buf[i]!='\0')
//            {
            fputc(buf[i],fp);
            total++;
//            }
        }
        
        prog(total, size);

    }
    fclose(fp);
    
}


void prog(int count, int max)
{
	char pre[] = "Progress: |";
	char post[] = "|";
    int newmax = 100;
    float counting = ((float)count / (float)max) * 100.0;
    int newcount = counting;
    

    printf("%s",pre);
    for(int i = 0; i < newcount; i++)
    {
        printf("=");
    }
    
    for(int d=newcount;d<newmax;d++)
    {
        printf(" ");
    }
    printf("%s\n",post);
}

void close_connection(int s)
{
    char buf[1000];
    
    // send QUIT
    sprintf(buf, "QUIT\n");
    send(s, buf, strlen(buf),0);
    printf("Goodbye\n");
    
    // Close socket
    close(s);
}